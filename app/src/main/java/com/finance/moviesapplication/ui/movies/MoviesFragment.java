package com.finance.moviesapplication.ui.movies;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.finance.moviesapplication.BuildConfig;
import com.finance.moviesapplication.MainActivity;
import com.finance.moviesapplication.R;
import com.finance.moviesapplication.model.Movie;
import com.finance.moviesapplication.model.Result;

import java.util.ArrayList;
import java.util.List;

public class MoviesFragment extends Fragment {

    private static final int COLUMN_COUNT = 2;
    private MoviesViewModel moviesViewModel;
    private MoviesAdapter adapter;
    private Observer<Result> movieObserver;
    private List<Movie> movieList = new ArrayList();
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private Context context;

    public MoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        moviesViewModel = new ViewModelProvider(this).get(MoviesViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movies_list, container, false);
        context = container.getContext();
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        progressBar = view.findViewById(R.id.progress);
        recyclerView.setLayoutManager(new GridLayoutManager(context, COLUMN_COUNT));
        adapter = new MoviesAdapter(movieList);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (MainActivity.isInternetAvailable(context)) {
            getMovieList();
        } else {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_SHORT).show();
        }
    }

    //Observe result of api .
    private void getMovieList() {
        progressBar.setVisibility(View.VISIBLE);
        if (movieObserver == null) {
            movieObserver = new Observer<Result>() {
                @Override
                public void onChanged(Result movies) {
                    progressBar.setVisibility(View.GONE);
                    if (movies != null) {
                        movieList.clear();
                        movieList.addAll(movies.getMovieList());
                        adapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, context.getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
                }
            };
        }
        moviesViewModel.getMovies(BuildConfig.API_KEY).observe(getViewLifecycleOwner(), movieObserver);
    }
}