package com.finance.moviesapplication.ui.movies;

import android.annotation.SuppressLint;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.finance.moviesapplication.model.Result;
import com.finance.moviesapplication.service.ApiClient;
import com.finance.moviesapplication.service.RestInterface;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MoviesViewModel extends ViewModel {
    MutableLiveData<Result> result = new MutableLiveData<>();

    // Get movies list from server
    @SuppressLint("CheckResult")
    public LiveData<Result> getMovies(String credential) {
        RestInterface apiInterface = ApiClient.getInstance().create(RestInterface.class);
        apiInterface.getMovies(credential)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Result>() {
                    @Override
                    public void onSuccess(Result nearVendorResponse) {
                        if (nearVendorResponse != null) {
                            result.postValue(nearVendorResponse);
                        } else {
                            result.postValue(null);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        result.postValue(null);
                    }
                });

        return result;
    }
}
