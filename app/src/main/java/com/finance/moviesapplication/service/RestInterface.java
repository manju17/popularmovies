package com.finance.moviesapplication.service;

import com.finance.moviesapplication.Constants;
import com.finance.moviesapplication.model.Result;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestInterface {

    @GET(Constants.PATH)
    Single<Result> getMovies(@Query(Constants.API_KEY) String key);
}
