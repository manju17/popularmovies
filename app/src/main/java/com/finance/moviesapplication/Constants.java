package com.finance.moviesapplication;

public class Constants {
    public static final String API_KEY = "api_key";
    public static final String PATH = "movie/popular";
}
